import './App.css';
import React from 'react';
import Dino from './components/Dino';
import Tour from './components/Tour'
import logo from "./img/Logo.png";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useLocation
} from 'react-router-dom';

function App() {
  return (

    <Router>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      <header id="header">
            <div> 
              <Link to={'/'}><img src={logo} class="box1" style={{width:'45px',height:'45px'}}></img></Link>       
            </div>
            <div class="box2"></div>
            <div class="box3" className="icons"><div class="fa fa-cart-plus"></div></div> 
            <div class="box4"  className="icons" ><div class="fa fa-user-o"></div></div>
        </header>


      <div className="App">
        <section>
          <Switch>
            <Route path="/exhibits">
              <div class="palka" style={{fontSize: ' 30px', color: '#fff', backgroundColor: '#088a3a', textAlign: 'left', paddingLeft: '15px', marginBottom: '10px'}}>Exhibits</div>
              <Tour /> 
              
            </Route>
            <Route path="/">
               <div id="section-1" >               
                <div style={{flex:1}}>
                    <Link to={'/exhibits'}><button className="item__button" style={{fontFamily: 'Solway-Bold', padding:'30px', margin: '500px 0 0 0 '}}>Exhibits</button></Link>
                </div>
                <div style={{flex:1.5, fontSize: '80px', color: '#fff', margin: '40px', fontFamily: 'Solway-Bold',}}>Welcome to museum of dinosaurs</div>       
              </div>
              <div class="palka" style={{fontSize: ' 30px', color: '#fff',backgroundColor: '#084200'}}>Mesozoic era</div>
              <Dino />
              <div class="palka" style={{fontSize: ' 30px', color: '#fff', backgroundColor: '#084200'}}>Cretaceous period</div>
              <Dino />
            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>
          <Route path="/exhibits">
              <Redirect to="/exhibits" />
          </Route>
        </section>      
      </div> 

     <footer style={{fontFamily: 'Teko-Light', fontSize: '22px'}}>
        <div id="name">
          <ul class="footer_left">
            <li>Dm.@gmail.com</li>
            <li>
              <h5><b>&copy;Dm, 2021</b></h5>
            </li>
            <li></li>
          </ul>
        </div>
        <div class="list">
          <ul>
            <li>Delivery</li>
            <li>Payment</li>
          </ul>
        </div>
        <div class="list">
          <ul>
            <li>News Dm</li>
            <li>Vacancies</li>
          </ul>
        </div>
     </footer>

    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default App;
