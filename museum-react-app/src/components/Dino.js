import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';
import dino1 from "../img/dino1.svg";
import "../css/Dino.css"

const api = new Api.DefaultApi()

class Dino extends React.Component {

    constructor(props) {
        super(props);
        const id =  this.props.match?.params.id || moment().format('LLLL');
        console.log(id);

        

        this.state = { 
            dino: [],
            date: id 
        };

        this.handleReload = this.handleReload.bind(this);
        //this.handleReload();
    }


    async handleReload(event) {
        const response = await api.dino({ date: ''/*this.state.targetDate*/ });
        this.setState({ dino: response});
    }


    render() {
        return <div>          
            <ul style={{fontSize: ' 25px', color: '#1F8873', dinoStyle: 'none', display:'flex'}}>
               {this.state.dino.map(
                   (dino) => 
                     <li style={{fontSize: ' 25px', color: 'black', padding:'40px', paddingBottom:'50px', float:'left', textAlign:'left', flex:'1'}} >
                                             <div style={{color: '#0023A0'}}> 
                                             <div> <img src={dino1}></img></div>
        </div>
        <div style={{fontSize: ' 30px', color: '#000000', fontFamily: 'Solway-Bold'}}>"{dino.name}"</div>
        <div  style={{fontFamily: 'Solway',fontSize: ' 20px'}}>{dino.name}, {dino.year} </div>
                        <div style={{fontFamily: 'Solway-Light',fontSize: ' 20px' }}> {dino.sentences} </div>
                        <div style={{fontFamily: 'Solway',fontSize: ' 20px'}}>{dino.dollar}</div>
                        <div style={{clear:'both'}}></div>
                    </li>)}
                    
             </ul>
             <div style={{clear:'both'}}></div>
            <div style={{padding:'10px'}}> <button className="Button2" style={{fontFamily: 'Pettaya', background: '#488440'}} onClick={this.handleReload}>Choose a tour!</button> </div>
        </div>
        
    }
}

export default withRouter(Dino);