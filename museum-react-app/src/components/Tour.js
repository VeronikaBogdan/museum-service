import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';
import image2 from "../img/image2.svg";
import "../css/Tour.css"

const api = new Api.DefaultApi()

class Tour extends React.Component {

    constructor(props) {
        super(props);
        const id =  this.props.match?.params.id || moment().format('LLLL');
        console.log(id);

        

        this.state = { 
            tour: [],
            date: id 
        };

        this.handleReload = this.handleReload.bind(this);
        //this.handleReload();
    }


    async handleReload(event) {
        const response = await api.tour({ date: ''/*this.state.targetDate*/ });
        this.setState({ tour: response});
    }


    render() {
        return <div>          
           <ul style={{fontSize: ' 25px', color: '#1F8873', /*bookStyle: 'none',*/ display:'flex', flexDirection: 'column', /*display: 'inline-block', position: 'relative', height: 'auto'*/}}>
            <div style={{fontSize: ' 25px', color: 'black',  paddingBottom:'20px',  textAlign:'center', flex:'1'}}><strong>Excursions for: </strong><Moment format="YYYY/MM/DD"></Moment></div>
            <div style={{clear:'both'}}></div>
               {this.state.tour.map(
                    (tour) => 
                         <li style={{fontSize: ' 25px', color: 'black',  paddingBottom:'20px',  textAlign:'center', flex:'1'}} >
                               <div style={{clear:'both'}}></div>
                             <div class="palka" style={{fontSize: ' 30px', color: '#fff', textAlign:'center', fontFamily: 'Solway'}}>"{tour.name}"</div>
                             <div style={{paddingLeft: '15px', display: 'flex', flexDirection:'column-reverse'}}>
                               
                                 <div style={{marginLeft: '25px'}}>
                                     <div style={{fontFamily: 'Solway', fontSize: '20px'}}>{tour.day}, {tour.address}, {tour.year} </div> 
                                     <div style={{fontFamily: 'Solway-Light',fontSize: '15px',  textAlign:'center'}}>{tour.description}</div>
                                     <div style={{clear:'both'}}></div>
                                     <button className="item__button1" style={{fontFamily: 'Solway-Bold'}}>{tour.dollar}</button>
                                 </div>
                                 <div style={{paddingTop: '10px', textAlign:'center' }}> <img src={image2}></img></div>
                             </div>
                         </li>)}
                    
             </ul>
             <div style={{clear:'both'}}></div>
            <div style={{padding:'10px'}}> <button className="Button2" style={{fontFamily: 'Pettaya'}} onClick={this.handleReload}>Learn more...</button> </div>
        </div>
        
    }
}     

export default withRouter(Tour);